# GNOME MPlayer
# Copyright (C) 2007 Kevin DeKorte
# This file is distributed under the same license as the GNOME MPlayer package.
# Kevin DeKorte <kdekorte@gmail.com> 2007.
#
#
msgid ""
msgstr ""
"Project-Id-Version: 0.9.6\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-09-16 12:46-0600\n"
"PO-Revision-Date: 2011-06-03 12:23+0000\n"
"Last-Translator: rob <linuxned@gmail.com>\n"
"Language-Team: Mark Huijgen <mark.sf.net@huijgen.tk>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2011-06-09 16:08+0000\n"
"X-Generator: Launchpad (build 13175)\n"

#: ../src/gm_audio.c:80 ../src/gm_audio.c:266
#: ../src/gmtk_output_combo_box.c:150
msgid "Default"
msgstr "Standaard"

#: ../src/gmtk_media_player.c:806 ../src/gmtk_media_player.c:1184
msgid "Frame Dropping"
msgstr ""

#: ../src/gmtk_media_player.c:820
msgid "Deinterlace"
msgstr ""

#: ../src/gmtk_media_player.c:1074
msgid "Loading..."
msgstr "Laden…"

#: ../src/gmtk_media_player.c:1212
msgid "Subtitles Visible"
msgstr ""

#: ../src/gmtk_media_player.c:1216
msgid "Subtitles Hidden"
msgstr ""

#: ../src/gmtk_media_player.c:3210
#, c-format
msgid "Failed to open %s"
msgstr "Kon %s niet openen"

#: ../src/gmtk_media_player.c:3252
msgid "Compressed SWF format not supported"
msgstr "Gecomprimeerd SWF-formaat wordt niet ondersteund"

#: ../src/gmtk_media_player.c:3361
#, c-format
msgid "Cache fill: %2.2f%%"
msgstr "Bufferniveau: %2.2f%%"

#: ../src/gmtk_media_player.c:3530 ../src/gmtk_media_player.c:3531
#: ../src/gmtk_media_player.c:3580 ../src/gmtk_media_player.c:3612
#: ../src/gmtk_media_player.c:3613 ../src/gmtk_media_player.c:3645
#: ../src/gmtk_media_player.c:3677
msgid "Unknown"
msgstr "Onbekend"

#: ../src/gmtk_media_player.c:3591
#, c-format
msgid "External Subtitle #%i"
msgstr "Externe ondertiteling #%i"

#: ../src/gmtk_media_player.c:3784
#, c-format
msgid "Screenshot saved to '%s'"
msgstr "Schermafdruk opgeslagen in '%s'"

#: ../src/gmtk_media_player.c:3787 ../src/gmtk_media_player.c:3797
#: ../src/gmtk_media_player.c:3927
msgid "GNOME MPlayer Notification"
msgstr "GNOME MPlayer-notificatie"

#: ../src/gmtk_media_player.c:3796
#, fuzzy
msgid "Failed to take screenshot"
msgstr "Kon %s niet openen"

#: ../src/gmtk_media_player.c:3924
msgid "MPlayer should be Upgraded to a Newer Version"
msgstr "MPlayer zou opgewaardeerd moeten worden naar een nieuwere versie"

#: ../src/gmtk_media_player.c:4190
#, fuzzy
msgid "You might want to consider upgrading mplayer to a newer version"
msgstr "U kunt overwegen om mplayer op te waarderen naar een nieuwere versie\n"

#: ../src/gmtk_media_tracker.c:102 ../src/gmtk_media_tracker.c:106
#: ../src/gmtk_media_tracker.c:216
msgid "No Information"
msgstr "Geen informatie"

#: ../src/gmtk_audio_meter.c:215
msgid "No Data"
msgstr ""

#~ msgid "GNOME MPlayer Error"
#~ msgstr "GNOME MPlayer-fout"
