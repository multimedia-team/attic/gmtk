# Finnish translation for gmtk
# Copyright (c) 2010 Rosetta Contributors and Canonical Ltd 2010
# This file is distributed under the same license as the gmtk package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2010.
#
msgid ""
msgstr ""
"Project-Id-Version: gmtk\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-09-16 12:46-0600\n"
"PO-Revision-Date: 2011-05-10 08:47+0000\n"
"Last-Translator: Jiri Grönroos <Unknown>\n"
"Language-Team: Finnish <fi@li.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Launchpad-Export-Date: 2011-06-09 16:08+0000\n"
"X-Generator: Launchpad (build 13175)\n"
"X-Poedit-Bookmarks: 7,-1,-1,-1,-1,-1,-1,-1,-1,-1\n"

#: ../src/gm_audio.c:80 ../src/gm_audio.c:266
#: ../src/gmtk_output_combo_box.c:150
msgid "Default"
msgstr "Oletus"

#: ../src/gmtk_media_player.c:806 ../src/gmtk_media_player.c:1184
msgid "Frame Dropping"
msgstr ""

#: ../src/gmtk_media_player.c:820
msgid "Deinterlace"
msgstr ""

#: ../src/gmtk_media_player.c:1074
msgid "Loading..."
msgstr "Ladataan..."

#: ../src/gmtk_media_player.c:1212
msgid "Subtitles Visible"
msgstr ""

#: ../src/gmtk_media_player.c:1216
msgid "Subtitles Hidden"
msgstr ""

#: ../src/gmtk_media_player.c:3210
#, c-format
msgid "Failed to open %s"
msgstr "Avaaminen epäonnistui %s"

#: ../src/gmtk_media_player.c:3252
msgid "Compressed SWF format not supported"
msgstr "Pakkaaminen SWF formaattiin ei ole tuettu"

#: ../src/gmtk_media_player.c:3361
#, c-format
msgid "Cache fill: %2.2f%%"
msgstr "Välimuisti käyttää: %2.2f%%"

#: ../src/gmtk_media_player.c:3530 ../src/gmtk_media_player.c:3531
#: ../src/gmtk_media_player.c:3580 ../src/gmtk_media_player.c:3612
#: ../src/gmtk_media_player.c:3613 ../src/gmtk_media_player.c:3645
#: ../src/gmtk_media_player.c:3677
msgid "Unknown"
msgstr "Tuntematon"

#: ../src/gmtk_media_player.c:3591
#, c-format
msgid "External Subtitle #%i"
msgstr ""

#: ../src/gmtk_media_player.c:3784
#, c-format
msgid "Screenshot saved to '%s'"
msgstr "Ruutukaappaus tallennettu '%s'"

#: ../src/gmtk_media_player.c:3787 ../src/gmtk_media_player.c:3797
#: ../src/gmtk_media_player.c:3927
msgid "GNOME MPlayer Notification"
msgstr "GNOME MPlayer Ilmoitus"

#: ../src/gmtk_media_player.c:3796
#, fuzzy
msgid "Failed to take screenshot"
msgstr "Avaaminen epäonnistui %s"

#: ../src/gmtk_media_player.c:3924
msgid "MPlayer should be Upgraded to a Newer Version"
msgstr "MPlayer tulisi päivittää uudempaan versioon"

#: ../src/gmtk_media_player.c:4190
msgid "You might want to consider upgrading mplayer to a newer version"
msgstr ""

#: ../src/gmtk_media_tracker.c:102 ../src/gmtk_media_tracker.c:106
#: ../src/gmtk_media_tracker.c:216
msgid "No Information"
msgstr "Ei informaatiota"

#: ../src/gmtk_audio_meter.c:215
msgid "No Data"
msgstr ""

#~ msgid "GNOME MPlayer Error"
#~ msgstr "GNOME MPlayer Virhe"
