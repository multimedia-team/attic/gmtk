gmtk (1.0.9-2) unstable; urgency=medium

  * debian/control:
    - Fix typo in package description. (Closes: #779979)
    - Bump Standards-Version.

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 25 Apr 2015 12:30:51 +0200

gmtk (1.0.9-1) unstable; urgency=medium

  * New upstream release.
  * debian/watch: Update for new download location.
  * debian/patches:
    - metadata-handling.patch, locale-agnostic.patch, input.conf-error.patch:
      Removed, all included upstream.
    - unbreak-ABI.patch: Move struct members around to not break the ABI.
  * debian/copyright: Update copyright years.
  * debian/control: Bump Standards-Version

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 12 Jun 2014 17:59:22 +0200

gmtk (1.0.8-3) unstable; urgency=low

  * debian/patches:
    - locale-agnostic.patch: Apply patch from upstream to pass floats in a
      locale agnostic way to mplayer. (LP: #1182874)
    - metadata-handling.patch: Apply patch from upstream to improve handling
      of metadata reported by mplayer.
    - input.conf-error.patch: Apply patch from upstream to not display an
      error message if input.conf could not be found.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 23 Jun 2013 17:43:49 +0200

gmtk (1.0.8-2) unstable; urgency=low

  * Upload to unstable.

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 06 Jun 2013 22:32:33 +0200

gmtk (1.0.8-1) experimental; urgency=low

  * New upstream release
    - Display an error message if taking a screenshot failed. (Closes:
      #699394)
  * debian/copyright: Update copyright years.

 -- Sebastian Ramacher <sramacher@debian.org>  Sun, 03 Mar 2013 18:37:17 +0100

gmtk (1.0.7-1) experimental; urgency=low

  * New upstream release.
  * debian/control:
    - Fix typos in in Description. Thanks to Mirco Scottà for the report.
      (Closes: #676279)
    - Bump Standards-Version to 3.9.4 (no changes required).
    - Update my address.
  * debian/patches: Refresh patches.
  * Bump SONAME:
    - Rename lib{gmlib,gmtk}0 to lib{gmlib,gmtk}1.
    - Rename libgmtk0-data to libgmtk1-data.
    - Rename lib{gmlib,gmtk}0-dbg to lib{gmlib,gmtk}1-dbg.
  * debian/*.symbols: Update symbols.
  * debian/copyright:
    - Add Hans Ecke.
    - Update my address

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 15 Nov 2012 22:32:39 +0100

gmtk (1.0.6-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright:
    - Add myself.
    - Update Format URL.
    - Remove unnecessary blocks.
  * debian/control:
    - Bump Standards version to 3.9.3 (no changes required).
    - Mark lib{gmlib,gmtk}-dev and lib{gmlib0,gmtk0}-dbg as Multi-Arch: same.
    - Bump debhelper Build-Dep to >= 9.
    - Drop unnecessary dpkg-dev Build-Dep.
  * debian/rules:
    - Enable verbose build output.
    - Enabled parallel builds.
    - Drop unnecessary include of /usr/share/dpkg/builflags.mk. debhelper >= 9
      does the right thing.
  * Update symbol files to add new symbols.
  * debian/patches:
    - Remove patch 0001-fix-buildsystem.patch (not needed anymore).
    - Add patch 0003-use-soname-as-domain.patch to use the SONAME as domain
      for gettext.
  * debian/libgmtk0-data.install: simplify.

 -- Sebastian Ramacher <s.ramacher@gmx.at>  Sun, 29 Apr 2012 18:45:38 +0200

gmtk (1.0.5-1) unstable; urgency=low

  * Initial release (Closes: #652680).

 -- Sebastian Ramacher <s.ramacher@gmx.at>  Thu, 05 Jan 2012 17:06:23 +0100
